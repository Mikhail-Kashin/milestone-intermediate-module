import { render } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';

import App from './app';

jest.mock('firebase/firestore', () => {
  return {
    getFirestore: jest.fn(),
  };
});
describe('app', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <MemoryRouter>
        <App />
      </MemoryRouter>
    );
    expect(baseElement).toBeTruthy();
  });
});
