import { Home, Favorites, NotFound } from '@react-v1/pages';
import { Route, Routes } from 'react-router-dom';
import { NavBar } from '@react-v1/components';

export function App() {
  return (
    <div className="App">
      <NavBar />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="*" element={<NotFound />} />
        <Route path="/favorites" element={<Favorites />} />
      </Routes>
    </div>
  );
}

export default App;
