import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom';
import { CssBaseline } from '@mui/material';
import { BrowserRouter } from 'react-router-dom';
import App from './app/app';

ReactDOM.render(
  <BrowserRouter>
    <StrictMode>
      <CssBaseline />
      <App />
    </StrictMode>
  </BrowserRouter>,
  document.getElementById('root')
);
