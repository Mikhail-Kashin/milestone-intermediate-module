module.exports = {
  displayName: 'yelp-lister',
  preset: '../../jest.preset.js',
  transform: {
    '^(?!.*\\.(js|jsx|ts|tsx|css|json)$)': '@nrwl/react/plugins/jest',
    '^.+\\.[tj]sx?$': 'babel-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  coverageDirectory: '../../coverage/apps/yelp-lister',
  collectCoverage: true,
  collectCoverageFrom: ['**/*.{ts,tsx}', '!**/environment*.ts'],
  coveragePathIgnorePatterns: ['<rootDir>/src/main.ts', '<rootDir>/src/polyfills.ts'],
  coverageReporters: ['json', 'lcov', 'text', 'clover'],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
};
