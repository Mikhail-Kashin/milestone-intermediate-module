import { render } from '@testing-library/react';

import Favorites from './favorites';

jest.mock('firebase/firestore', () => {
  return {
    getFirestore: jest.fn(),
  };
});
describe('Favorites', () => {
  it('should render succesfully', () => {
    const { baseElement } = render(<Favorites />);
    expect(baseElement).toBeTruthy();
  });
});
