import { Grid, Typography } from '@mui/material';
import { BusinessesList } from '@react-v1/components';

import { useFavorites } from '@react-v1/hooks';

export function Favorites() {
  const { removeBusinessFromFavorites, addBusinessToFavorites, favorites } =
    useFavorites();

  return (
    <Grid>
      <Grid
        sx={{
          padding: '1rem 2rem 2.4rem',
        }}
      >
        <Typography
          sx={{
            textAlign: 'center',
            fontSize: '2rem',
            marginBottom: '1rem',
            fontWeight: '600',
          }}
        >
          Your Favorites!
        </Typography>
      </Grid>
      <BusinessesList
        businesses={favorites}
        favorites={favorites}
        addFavorite={addBusinessToFavorites}
        removeFavorite={removeBusinessFromFavorites}
      />
    </Grid>
  );
}

export default Favorites;
