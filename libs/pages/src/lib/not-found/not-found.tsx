import { Stack, Typography } from '@mui/material';

export function NotFound() {
  return (
    <Stack alignItems="center" sx={{ padding: 15 }}>
      <Typography>Page not found!</Typography>
    </Stack>
  );
}

export default NotFound;
