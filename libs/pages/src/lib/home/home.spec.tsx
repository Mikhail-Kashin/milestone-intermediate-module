import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import * as dataAccess from '@react-v1/data-access';
import {
  transformedFavorites,
  yelpBusinessSearchData,
} from '@react-v1/mock-data';
import { transformBusinesses } from '@react-v1/utilities';

import Home from './home';

const businesses = transformBusinesses(yelpBusinessSearchData.businesses);

afterEach(() => {
  jest.resetAllMocks();
});

jest.mock('firebase/app', () => ({
  initializeApp: () => ({}),
}));

jest.mock('firebase/firestore', () => ({
  getFirestore: () => ({}),
}));

describe('home', () => {
  it('should render successfully', async () => {
    jest
      .spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise((res) => res(transformedFavorites)));

    const { baseElement } = render(<Home />);
    const title = await screen.findByText('Yelp Lister');
    expect(baseElement).toBeTruthy();
    expect(title).toBeTruthy();
  });

  it('should render a list of businesses', async () => {
    jest
      .spyOn(dataAccess, 'getFavorites')
      .mockReturnValueOnce(new Promise((res) => res(transformedFavorites)));

    jest
      .spyOn(dataAccess, 'getBusinesses')
      .mockReturnValueOnce(new Promise((res) => res(businesses)));

    render(<Home />);
    const searchInput = screen.getByRole('searchbox');
    userEvent.type(searchInput, 'jacksonville {enter}');
    const girlAndGoat = await screen.findByText('Girl & The Goat');
    const purplePig = await screen.findByText('The Purple Pig');
    expect(girlAndGoat).toBeTruthy();
    expect(purplePig).toBeTruthy();
  });
});
