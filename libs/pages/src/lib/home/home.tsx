import { useState, useEffect, useMemo, useCallback } from 'react';
import {
  Grid,
  SelectChangeEvent,
  Stack,
  Typography,
  CircularProgress,
} from '@mui/material';
import {
  BusinessesList,
  SearchInput,
  CategoryDropdown,
  ErrorPage,
} from '@react-v1/components';
import { getBusinesses } from '@react-v1/data-access';
import { Business } from '@react-v1/types';
import { createCategoryArray } from '@react-v1/utilities';
import { useFavorites } from '@react-v1/hooks';

export function Home() {
  const [error, setError] = useState<string | null>(null);
  const [businesses, setBusinesses] = useState<Business[]>([]);
  const [categorizedBusinesses, setCategorizedBusinesses] = useState<
    Business[]
  >([]);
  const [categories, setCategories] = useState<string[]>([]);
  const [selectedCategories, setSelectedCategories] = useState<string[]>([]);
  const [isLoading, setIsLoading] = useState(false);

  const { removeBusinessFromFavorites, addBusinessToFavorites, favorites } =
    useFavorites();

  const onSearch = async (value: string | null) => {
    setIsLoading(true);
    setSelectedCategories([]);
    if (value) {
      const businessesResponse = await getBusinesses(value);
      if (businessesResponse instanceof Error) {
        setError(businessesResponse.message);
        setIsLoading(false);
        setBusinesses([]);
        setCategorizedBusinesses([]);
        return;
      }
      setBusinesses(businessesResponse);
      setError(null);
      setCategorizedBusinesses([]);
    }
    setIsLoading(false);
  };

  const memoizedHandleCategory = useCallback(
    (event: SelectChangeEvent<string[]>) => {
      const value = event.target.value;
      const selected = typeof value === 'string' ? value.split(',') : value;
      const filteredBusinesses = businesses.filter((business) => {
        return business.categories.some((category) =>
          selected.includes(category)
        );
      });
      setSelectedCategories(selected);
      setCategorizedBusinesses(filteredBusinesses);
    },
    [businesses]
  );

  const memoizedCategorizedBusinesses = useMemo(() => {
    const list = categorizedBusinesses.length
      ? categorizedBusinesses
      : businesses;
    return list;
  }, [businesses, categorizedBusinesses]);

  useEffect(() => {
    if (error) setCategories([]);
    if (businesses.length > 0) {
      setCategories(createCategoryArray(businesses));
    }
  }, [businesses, error]);

  return (
    <Grid>
      <Grid
        sx={{
          padding: '1rem 2rem 2.4rem',
        }}
      >
        <Typography
          sx={{
            textAlign: 'center',
            fontSize: '2rem',
            marginBottom: '1rem',
            fontWeight: '600',
          }}
        >
          Yelp Lister
        </Typography>
        <Stack direction="row">
          <SearchInput onSearch={onSearch} />
          <CategoryDropdown
            categories={categories}
            onHandleCategory={memoizedHandleCategory}
            selectedCategories={selectedCategories}
          />
        </Stack>
        {isLoading && <CircularProgress />}
        {error && <ErrorPage message={error} />}
      </Grid>
      <BusinessesList
        businesses={memoizedCategorizedBusinesses}
        favorites={favorites}
        addFavorite={addBusinessToFavorites}
        removeFavorite={removeBusinessFromFavorites}
      />
    </Grid>
  );
}

export default Home;
