module.exports = {
  displayName: 'pages',
  preset: '../../jest.preset.js',
  transform: {
    '^.+\\.[tj]sx?$': 'babel-jest',
  },
  moduleFileExtensions: ['ts', 'tsx', 'js', 'jsx'],
  coverageDirectory: '../../coverage/libs/components',
  collectCoverage: true,
  collectCoverageFrom: ['**/*.{ts,tsx}', '!**/*.stories.tsx'],
  coveragePathIgnorePatterns: ['<rootDir>/src/index.ts'],
  coverageReporters: ['json', 'lcov', 'text', 'clover'],
  coverageThreshold: {
    global: {
      branches: 80,
      functions: 80,
      lines: 80,
      statements: 80,
    },
  },
};
