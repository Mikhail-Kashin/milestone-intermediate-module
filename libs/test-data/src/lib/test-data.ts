import { Business } from '@react-v1/types';

export const mockCategories = [
  'Southern',
  'Breakfast & Brunch',
  'Comfort Food',
  'Bakeries',
  'Sandwiches',
  'Salad',
];

export const duplicatedMockCategories = [
  'Southern',
  'Southern',
  'Breakfast & Brunch',
  'Comfort Food',
  'Bakeries',
  'Sandwiches',
  'Salad',
];

export const rawMockedBussinesses = [
  {
    id: 'Wb8-2onR2nzR1YnVlv4MYQ',
    alias: 'julington-creek-fish-camp-jacksonville',
    name: 'Julington Creek Fish Camp',
    image_url:
      'https://s3-media3.fl.yelpcdn.com/bphoto/005KpezioMKH_R3nFceFDQ/o.jpg',
    is_closed: false,
    url: 'https://www.yelp.com/biz/julington-creek-fish-camp-jacksonville?adjust_creative=f6vhwAod4mFlX_Xq0UUEAg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=f6vhwAod4mFlX_Xq0UUEAg',
    review_count: 809,
    categories: [
      {
        alias: 'seafood',
        title: 'Seafood',
      },
      {
        alias: 'newamerican',
        title: 'American (New)',
      },
      {
        alias: 'southern',
        title: 'Southern',
      },
    ],
    rating: 4,
    coordinates: {
      latitude: 30.1343788152894,
      longitude: -81.6321039944887,
    },
    transactions: ['pickup', 'delivery'],
    price: '$$',
    location: {
      address1: '12760 San Jose Blvd',
      address2: '',
      address3: '',
      city: 'Jacksonville',
      zip_code: '32223',
      country: 'US',
      state: 'FL',
      display_address: ['12760 San Jose Blvd', 'Jacksonville, FL 32223'],
    },
    phone: '+19048862267',
    display_phone: '(904) 886-2267',
    distance: 2667.178919956493,
  },
  {
    id: 'q58FuJDqd6fNHw1W46spBg',
    alias: 'hana-moon-sushi-jacksonville',
    name: 'Hana Moon Sushi',
    image_url:
      'https://s3-media2.fl.yelpcdn.com/bphoto/eKDdIX87GJe2w4e-zZODIA/o.jpg',
    is_closed: false,
    url: 'https://www.yelp.com/biz/hana-moon-sushi-jacksonville?adjust_creative=f6vhwAod4mFlX_Xq0UUEAg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=f6vhwAod4mFlX_Xq0UUEAg',
    review_count: 156,
    categories: [
      {
        alias: 'sushi',
        title: 'Sushi Bars',
      },
      {
        alias: 'ramen',
        title: 'Ramen',
      },
    ],
    rating: 5,
    coordinates: {
      latitude: 30.16415,
      longitude: -81.63206,
    },
    transactions: ['pickup', 'delivery'],
    price: '$$',
    location: {
      address1: '11531-09 San Jose Blvd',
      address2: null,
      address3: '',
      city: 'Jacksonville',
      zip_code: '32223',
      country: 'US',
      state: 'FL',
      display_address: ['11531-09 San Jose Blvd', 'Jacksonville, FL 32223'],
    },
    phone: '+19044230071',
    display_phone: '(904) 423-0071',
    distance: 1445.832641257848,
  },
];

export const mockBusiness = {
  id: 'GHsoAez7pyAf8d6Uz7N1JA',
  alias: 'maple-street-biscuit-company-san-marco-jacksonville',
  name: 'Maple Street Biscuit Company - San Marco',
  image_url:
    'https://s3-media4.fl.yelpcdn.com/bphoto/Szb7WihIfj3Kx39mYfEwFg/o.jpg',
  is_closed: false,
  url: 'https://www.yelp.com/biz/maple-street-biscuit-company-san-marco-jacksonville?adjust_creative=f6vhwAod4mFlX_Xq0UUEAg&utm_campaign=yelp_api_v3&utm_medium=api_v3_business_search&utm_source=f6vhwAod4mFlX_Xq0UUEAg',
  review_count: 1019,
  categories: [
    {
      alias: 'southern',
      title: 'Southern',
    },
    {
      alias: 'breakfast_brunch',
      title: 'Breakfast & Brunch',
    },
    {
      alias: 'comfortfood',
      title: 'Comfort Food',
    },
  ],
  rating: 4.5,
  coordinates: {
    latitude: 30.3038463782095,
    longitude: -81.6539008666204,
  },
  transactions: ['delivery'],
  price: '$',
  location: {
    address1: '2004 San Marco Blvd',
    address2: '',
    address3: '',
    city: 'Jacksonville',
    zip_code: '32204',
    country: 'US',
    state: 'FL',
    display_address: ['2004 San Marco Blvd', 'Jacksonville, FL 32204'],
  },
  phone: '+19043981004',
  display_phone: '(904) 398-1004',
  distance: 3951.7254377961917,
};

export const mockBusinesses: Business[] = [
  {
    id: 'GHsoAez7pyAf8d6Uz7N1JA',
    name: 'Maple Street Biscuit Company - San Marco',
    image:
      'https://s3-media4.fl.yelpcdn.com/bphoto/lVcTfQ3PEESoNgxlPnLHZQ/o.jpg',
    isClosed: false,
    categories: ['Southern', 'Breakfast & Brunch', 'Comfort Food'],
    rating: 4.5,
    address: ['2004 San Marco Blvd', 'Jacksonville, FL 32204'],
    phone: '(904) 398-1004',
  },
  {
    id: 'PeuV15g1xxW_hJGvtQPMrg',
    name: 'The French Pantry',
    image:
      'https://s3-media4.fl.yelpcdn.com/bphoto/JIfYQCuKOGFFBo8pREmozA/o.jpg',
    isClosed: false,
    categories: ['Bakeries', 'Sandwiches', 'Salad'],
    rating: 4.5,
    address: ['6301 Powers Ave', 'Jacksonville, FL 32217'],
    phone: '(904) 730-8696',
  },
];

export const mockBusinessesWithDuplicatedCategories: Business[] = [
  {
    id: 'GHsoAez7pyAf8d6Uz7N1JA',
    name: 'Maple Street Biscuit Company - San Marco',
    image:
      'https://s3-media4.fl.yelpcdn.com/bphoto/lVcTfQ3PEESoNgxlPnLHZQ/o.jpg',
    isClosed: false,
    categories: ['Southern', 'Breakfast & Brunch', 'Comfort Food'],
    rating: 4.5,
    address: ['2004 San Marco Blvd', 'Jacksonville, FL 32204'],
    phone: '(904) 398-1004',
  },
  {
    id: 'PeuV15g1xxW_hJGvtQPMrg',
    name: 'The French Pantry',
    image:
      'https://s3-media4.fl.yelpcdn.com/bphoto/JIfYQCuKOGFFBo8pREmozA/o.jpg',
    isClosed: false,
    categories: ['Bakeries', 'Sandwiches', 'Salad', 'Southern'],
    rating: 4.5,
    address: ['6301 Powers Ave', 'Jacksonville, FL 32217'],
    phone: '(904) 730-8696',
  },
];
