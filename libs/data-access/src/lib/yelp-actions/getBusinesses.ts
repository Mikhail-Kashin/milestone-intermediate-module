import axios, { AxiosError } from 'axios';
import { transformBusinesses } from '@react-v1/utilities';
import { Business } from '@react-v1/types';

export async function getBusinesses(
  query: string
): Promise<Business[] | Error> {
  const corsApiUrl = 'https://cors-anywhere.herokuapp.com/';
  const yelpApiUrl = `https://api.yelp.com/v3/businesses/search?location=${query}`;
  const yelpAPIAUTH = process.env['NX_YELP_API_KEY'];

  try {
    const res = await axios.get(`${corsApiUrl}${yelpApiUrl}`, {
      headers: {
        Authorization: `Bearer ${yelpAPIAUTH}`,
      },
    });

    const businesses = res.data.businesses;

    return transformBusinesses(businesses);
  } catch (err) {
    if (err instanceof AxiosError && err.response) {
      return new Error(err.response.data.error.description);
    }
    return err as Error;
  }
}

export default getBusinesses;
