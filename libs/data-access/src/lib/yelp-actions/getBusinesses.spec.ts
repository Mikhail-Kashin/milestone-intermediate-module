import { rawMockedBussinesses } from '@react-v1/test-data';
import getBusinesses from './getBusinesses';
import axios from 'axios';

afterEach(() => {
  jest.resetAllMocks();
});

describe('getBusinesses', () => {
  it('should return a business object', async () => {
    jest
      .spyOn(axios, 'get')
      .mockResolvedValueOnce({ data: { businesses: rawMockedBussinesses } });

    const resp = await getBusinesses('jacksonville');

    expect(resp[0].name).toBe(rawMockedBussinesses[0].name);
  });
});
