import { removeFavorite } from './removeFavorite'

afterEach(() => {
  jest.resetAllMocks()
})

describe('removeFavorite', () => {
  it('should return a success message on successful requests', async () => {
    jest.mock('firebase/firestore', () => ({
      deleteDoc: () => new Promise(res => res(true))
    }))
    const resp = await removeFavorite('firebase id')
    expect(resp.message).toBe('success')
  })

  it('should return an error message if id is not a string', async () => {
    // @ts-expect-error testing invalid argument type
    const error = await removeFavorite(42)
    expect(error).toBeInstanceOf(Error)
  })
})
