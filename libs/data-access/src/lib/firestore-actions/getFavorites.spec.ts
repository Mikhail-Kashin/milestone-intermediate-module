import { getFavorites } from './getFavorites';
import { transformedFavorites } from '@react-v1/mock-data';
import { Favorite } from '@react-v1/types';
import { QuerySnapshot } from 'firebase/firestore';

afterEach(() => {
  jest.resetAllMocks();
});

describe('getFavorites', () => {
  it('should return an array of favorites on successful requests', async () => {
    const querySnapshot: QuerySnapshot = {
      // @ts-expect-error just mocking what's used in getFavorites
      docs: transformedFavorites,
      // @ts-expect-error just mocking what's used in getFavorites
      forEach: function (callbackFn: (doc: Favorite) => void) {
        this.docs.forEach(callbackFn);
      },
    };
    jest.mock('firebase/firestore', () => ({
      getDocs: () => new Promise((res) => res(querySnapshot)),
    }));
    const favorites = await getFavorites();
    const favorite = favorites[0];
    expect(favorite.firebaseId).toBeTruthy();
  });
});
