import { Favorite } from '@react-v1/types'
import { collection, getDocs } from 'firebase/firestore'
import { db, favoritesPath } from '../firestore'

export const getFavorites = async (): Promise<Favorite[] | Error> => {
  try {
    const querySnapshot = await getDocs(collection(db, favoritesPath))
    const favorites: Favorite[] = []
    querySnapshot.forEach(doc => {
      favorites.push({
        ...doc.data(),
        firebaseId: doc.id,
      } as Favorite)
    })
    return favorites
  } catch(error) {
    return error as Error
  }
}
