import { Business } from '@react-v1/types'
import { isValidBusiness } from '@react-v1/utilities'
import { collection, addDoc } from 'firebase/firestore'
import { db, favoritesPath } from '../firestore'

export const addFavorite = async (business: Business) => {
  try {
    if(!isValidBusiness(business)) {
      throw new Error('business is not valid')
    }
    await addDoc(collection(db, favoritesPath), business)
    return { message: 'success' }
  } catch(error) {
    return error
  }
}
