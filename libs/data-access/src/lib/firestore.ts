import { initializeApp } from 'firebase/app'
import { getFirestore } from 'firebase/firestore'

export const firebaseConfig = {
  apiKey: 'AIzaSyCcqh--P_DrDrqnCU-VPkQ_EMeRk53UQN4',
  authDomain: 'mp-react-v1.firebaseapp.com',
  databaseURL: 'https://mp-react-v1-default-rtdb.firebaseio.com',
  projectId: 'mp-react-v1',
  storageBucket: 'mp-react-v1.appspot.com',
  messagingSenderId: '86983335574',
  appId: '1:86983335574:web:5db0b321aa5795ac3d2cb7'
}

export const app = initializeApp(firebaseConfig)
export const db = getFirestore()

export const favoritesPath = 'favorites'
