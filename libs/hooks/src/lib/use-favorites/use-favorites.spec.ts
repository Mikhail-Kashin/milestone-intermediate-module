import { renderHook } from '@testing-library/react-hooks';
import { useFavorites } from './use-favorites';
import { waitFor } from '@testing-library/react';
import * as dataAccess from '@react-v1/data-access';
import { transformedFavorites } from '@react-v1/mock-data';

jest.mock('firebase/firestore', () => {
  return {
    getFirestore: jest.fn(),
  };
});

beforeEach(() => {
  jest.resetAllMocks();
});

describe('useFavorites', () => {
  it('should allow the user to favorite a business', async () => {
    jest
      .spyOn(dataAccess, 'getFavorites')
      .mockResolvedValue(transformedFavorites);

    const { result } = renderHook(() => useFavorites());
    expect(result.current.favorites).toHaveLength(0);
    await waitFor(() => expect(result.current.favorites).toHaveLength(1));
  });

  it('should be able to add a new favorite', async () => {
    jest.spyOn(dataAccess, 'getFavorites').mockResolvedValue([]);
    const addToFavorites = jest
      .spyOn(dataAccess, 'addFavorite')
      .mockResolvedValue(transformedFavorites[0]);

    const { result } = renderHook(() => useFavorites());

    await result.current.addBusinessToFavorites(transformedFavorites[0]);

    expect(addToFavorites).toHaveBeenCalledTimes(1);
    expect(addToFavorites).toHaveBeenCalledWith(transformedFavorites[0]);
  });

  it('should be able to remove from favorites', async () => {
    jest
      .spyOn(dataAccess, 'getFavorites')
      .mockResolvedValue(transformedFavorites);
    const removeFavorite = jest
      .spyOn(dataAccess, 'removeFavorite')
      .mockResolvedValue([]);

    const { result } = renderHook(() => useFavorites());

    await result.current.removeBusinessFromFavorites('-Mxv7M-Ysw_H6uIlNawq');

    expect(removeFavorite).toHaveBeenCalledTimes(1);
    expect(removeFavorite).toHaveBeenCalledWith('-Mxv7M-Ysw_H6uIlNawq');
  });
});
