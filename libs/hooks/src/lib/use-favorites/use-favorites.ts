import { useState, useEffect } from 'react';
import { Business, Favorite } from '@react-v1/types';
import {
  addFavorite,
  getFavorites,
  removeFavorite,
} from '@react-v1/data-access';

export function useFavorites() {
  const [favorites, setFavorites] = useState<Favorite[]>([]);

  const addBusinessToFavorites = async (business: Business) => {
    setFavorites([...favorites, { ...business, firebaseId: 'temp' }]);
    const addFavoriteResp = await addFavorite(business);
    if (addFavoriteResp instanceof Error) {
      setFavorites(favorites);
      return;
    }
    const updatedFavorites = await getFavorites();
    if (updatedFavorites instanceof Error) return;
    setFavorites(updatedFavorites);
  };

  const removeBusinessFromFavorites = async (firebaseId: string) => {
    setFavorites(
      favorites.filter((favorite) => favorite.firebaseId !== firebaseId)
    );
    const removeFavoriteResp = await removeFavorite(firebaseId);
    if (removeFavoriteResp instanceof Error) {
      setFavorites(favorites);
      return;
    }
    const updatedFavorites = await getFavorites();
    if (updatedFavorites instanceof Error) return;
    setFavorites(updatedFavorites);
  };

  useEffect(() => {
    getFavorites().then((favoritesData) => {
      if (favoritesData instanceof Error) return;
      setFavorites(favoritesData);
    });
  }, []);

  return { removeBusinessFromFavorites, addBusinessToFavorites, favorites };
}

export default useFavorites;
