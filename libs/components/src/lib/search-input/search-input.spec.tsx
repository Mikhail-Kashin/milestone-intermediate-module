import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event'

import SearchInput from './search-input';

describe('SearchInput', () => {
  it('should render successfully', () => {
    const onSearch = jest.fn()
    const { baseElement } = render(<SearchInput onSearch={onSearch}/>);
    expect(baseElement).toBeTruthy();
  });

  it('should accept user inputted text', () => {
    const onSearch = jest.fn()
    render(<SearchInput onSearch={onSearch}/>);
    const searchInput = screen.getByRole('searchbox')
    userEvent.type(searchInput, 'search')
    expect(screen.getByDisplayValue('search')).toBeTruthy()
  })

  it('should run onSearch function on form submit', () => {
    const onSearch = jest.fn()
    render(<SearchInput onSearch={onSearch}/>);
    const searchInput = screen.getByRole('searchbox')
    userEvent.type(searchInput, 'search{enter}')
    expect(onSearch).toBeCalledTimes(1)
  })

  it('should clear filter value if search input is empty', () => {
    const onSearch = jest.fn(input => input)
    render(<SearchInput onSearch={onSearch}/>);
    const searchInput = screen.getByRole('searchbox') as HTMLInputElement
    userEvent.type(searchInput, 'search{enter}')
    searchInput.setSelectionRange(0, 6)
    userEvent.type(searchInput, '{del}')
    expect(onSearch).toBeCalledTimes(2)
  })
});
