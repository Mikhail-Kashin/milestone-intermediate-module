import { Stack, Typography } from '@mui/material';

interface ErrorPageProps {
  message: string;
}

export function ErrorPage({ message }: ErrorPageProps) {
  return (
    <Stack alignItems="center" sx={{ padding: 4 }}>
      <Typography>{message}</Typography>
    </Stack>
  );
}

export default ErrorPage;
