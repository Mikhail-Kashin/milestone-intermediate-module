import { render, screen } from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import CategoryDropdown from './category-dropdown';
import { mockCategories } from '@react-v1/test-data';

const defaultProps = {
  categories: [...mockCategories],
  onHandleCategory: jest.fn(),
  selectedCategories: [...[mockCategories[0], ...mockCategories[1]]],
};

beforeEach(() => {
  jest.resetAllMocks();
  jest.clearAllMocks();
});

describe('CategoryDropdown', () => {
  it('should render successfully and populates dropdown', () => {
    const { baseElement } = render(<CategoryDropdown {...defaultProps} />);

    const categoryDropDown = screen.getByRole('button');

    expect(categoryDropDown).toBeTruthy();

    expect(baseElement).toBeTruthy();
  });

  it('should allow dropdown to be clicked', async () => {
    render(<CategoryDropdown {...defaultProps} />);
    const categoryDropDown = screen.getByRole('button');

    userEvent.click(categoryDropDown);

    expect(defaultProps.onHandleCategory).toBeCalledTimes(0);

    const categorySelection = screen.getAllByRole(
      'option'
    ) as HTMLOptionElement[];
    userEvent.click(categorySelection[0]);

    expect(categorySelection[0].selected).toBe(true);
    expect(defaultProps.onHandleCategory).toBeCalledTimes(1);
  });
});
