import {
  FormControl,
  InputLabel,
  Select,
  MenuItem,
  SelectChangeEvent,
  Checkbox,
  ListItemText,
  OutlinedInput,
} from '@mui/material';

export interface CategoryDropdownProps {
  categories: string[];
  onHandleCategory: (value: SelectChangeEvent<string[]>) => void;
  selectedCategories: string[];
}

export function CategoryDropdown({
  categories,
  onHandleCategory,
  selectedCategories,
}: CategoryDropdownProps) {
  return (
    <div>
      <FormControl
        sx={{
          width: 200,
          '& .MuiInputBase-input': {
            paddingLeft: '0.6rem',
          },
          marginLeft: 3,
        }}
      >
        <InputLabel>Category</InputLabel>
        <Select
          multiple
          value={selectedCategories}
          onChange={onHandleCategory}
          input={<OutlinedInput label="Category" />}
          renderValue={(selectedCategories) => selectedCategories.join(', ')}
        >
          {categories.map((category) => {
            const isChecked = selectedCategories.includes(category);
            return (
              <MenuItem key={category} value={category}>
                <Checkbox checked={isChecked} />
                <ListItemText primary={category} />
              </MenuItem>
            );
          })}
        </Select>
      </FormControl>
    </div>
  );
}

export default CategoryDropdown;
