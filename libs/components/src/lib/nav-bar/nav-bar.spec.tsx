import { render, screen } from '@testing-library/react';
import { MemoryRouter } from 'react-router-dom';
import userEvent from '@testing-library/user-event';

import NavBar from './nav-bar';

afterEach(() => {
  jest.resetAllMocks();
});

const mockedNavigate = jest.fn();
jest.mock('react-router-dom', () => ({
  ...jest.requireActual('react-router-dom'),
  useNavigate: () => mockedNavigate,
}));

describe('NavBar', () => {
  it('should render successfully', () => {
    const { baseElement } = render(
      <MemoryRouter>
        <NavBar />
      </MemoryRouter>
    );
    expect(baseElement).toBeTruthy();
  });

  it('should allow user to navigate to the home page', () => {
    render(
      <MemoryRouter>
        <NavBar />
      </MemoryRouter>
    );

    const homeButton = screen.getByRole('button', { name: 'Home' });
    userEvent.click(homeButton);
    expect(mockedNavigate).toBeCalledTimes(1);
  });
});
