import {
  AppBar,
  Box,
  Stack,
  Toolbar,
  ListItem,
  ListItemText,
  ListItemButton,
} from '@mui/material';
import { useNavigate, generatePath } from 'react-router-dom';
import { ROUTES } from '@react-v1/config';

export function NavBar() {
  const pages = [
    {
      text: 'Home',
      route: generatePath(ROUTES.HOME),
    },
    {
      text: 'Favorites',
      route: generatePath(ROUTES.FAVORITES),
    },
  ];

  const navigate = useNavigate();
  return (
    <Box sx={{ display: 'flex' }}>
      <AppBar position="fixed">
        <Stack alignItems="left" justifyContent="center">
          <Toolbar>
            {pages.map(({ text, route }) => (
              <ListItem key={text} disablePadding sx={{ maxWidth: 100 }}>
                <ListItemButton onClick={() => navigate(route)}>
                  <ListItemText primary={text} />
                </ListItemButton>
              </ListItem>
            ))}
          </Toolbar>
        </Stack>
      </AppBar>
    </Box>
  );
}

export default NavBar;
