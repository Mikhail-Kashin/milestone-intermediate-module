import { render } from '@testing-library/react';
import FavoriteButton from './favorite-button';

describe('FavoriteButton', () => {
  it('should render successfully when not favorited', () => {
    const onFavoriteClick = jest.fn()
    const { baseElement } = render(
      <FavoriteButton
        addFavorite={onFavoriteClick}
        removeFavorite={onFavoriteClick}
        isFavorite={false}
      />
    );
    expect(baseElement).toBeTruthy();
  });

  it('should render successfully when favorited', () => {
    const onFavoriteClick = jest.fn()
    const { baseElement } = render(
      <FavoriteButton
        addFavorite={onFavoriteClick}
        removeFavorite={onFavoriteClick}
        isFavorite={true}
      />
    );
    expect(baseElement).toBeTruthy();
  });
});
