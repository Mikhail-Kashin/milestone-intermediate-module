import { Favorite, FavoriteBorder } from '@mui/icons-material';
import { IconButton } from '@mui/material';

export interface FavoriteButtonProps {
  addFavorite: () => void;
  removeFavorite: () => void;
  isFavorite: boolean;
}

export function FavoriteButton({
  addFavorite,
  removeFavorite,
  isFavorite,
}: FavoriteButtonProps) {
  const onClick = isFavorite ? removeFavorite : addFavorite;
  return (
    <IconButton onClick={onClick} sx={{ color: 'red' }}>
      {isFavorite ? (
        <Favorite data-testid="favorite-btn-filled" />
      ) : (
        <FavoriteBorder data-testid="favorite-btn-empty" />
      )}
    </IconButton>
  );
}

export default FavoriteButton;
