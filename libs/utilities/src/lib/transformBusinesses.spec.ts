import { transformBusinesses } from './transformBusinesses';
import { yelpBusinessSearchData } from '@react-v1/mock-data'

describe('transformBusinesses', () => {
  it('should transform yelp API business data model to app data model', () => {
    const business = transformBusinesses(yelpBusinessSearchData.businesses)[0]
    expect(business.id).toBeDefined();
    expect(business.name).toBeDefined();
    expect(business.image).toBeDefined();
    expect(business.isClosed).toBeDefined();
    expect(business.categories).toBeDefined();
    expect(business.rating).toBeDefined();
    expect(business.address).toBeDefined();
    expect(business.phone).toBeDefined();
  });
});
