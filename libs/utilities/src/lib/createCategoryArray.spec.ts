import { createCategoryArray } from './createCategoryArray';
import { Business } from '@react-v1/types';
import {
  mockBusinesses,
  mockCategories,
  duplicatedMockCategories,
  mockBusinessesWithDuplicatedCategories,
} from '@react-v1/test-data';

describe('createCategoryArray', () => {
  it('should create an array of categories', () => {
    const categoryArray = createCategoryArray(mockBusinesses);
    expect(categoryArray).toEqual(mockCategories);
  });

  it('shouldnt duplicate categories', () => {
    const categoryArray = createCategoryArray(
      mockBusinessesWithDuplicatedCategories
    );
    expect(categoryArray).not.toEqual(duplicatedMockCategories);
  });
});
