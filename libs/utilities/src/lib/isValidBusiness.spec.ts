import { isValidBusiness } from './isValidBusiness'
import { yelpBusinessSearchData } from '@react-v1/mock-data'
import { transformBusinesses } from './transformBusinesses'
import { Business } from '@react-v1/types'

const businesses = transformBusinesses(yelpBusinessSearchData.businesses)

describe('isValidBusiness', () => {
  test('should return true when given a valid business', () => {
    const isValid = isValidBusiness(businesses[0])
    expect(isValid).toBeTruthy()
  })

  test('should return false when given an invalid business', () => {
    const isNotValid = isValidBusiness({} as Business)
    expect(isNotValid).toBeFalsy()
  })
})