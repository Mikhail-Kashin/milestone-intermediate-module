import { yelpBusinessSearchData } from '@react-v1/mock-data'

export const transformBusinesses = (businesses: typeof yelpBusinessSearchData.businesses) => {
  return businesses.map(({
    id,
    name,
    image_url: image,
    is_closed: isClosed,
    categories,
    rating,
    location: { display_address: address },
    display_phone: phone
  }) => ({
    id,
    name,
    image,
    isClosed,
    categories: categories.map(category => category.title),
    rating,
    address,
    phone
  }))
}
