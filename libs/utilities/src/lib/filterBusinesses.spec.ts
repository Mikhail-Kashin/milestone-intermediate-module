import { filterBusinesses } from './filterBusinesses';
import { transformBusinesses } from './transformBusinesses';
import { yelpBusinessSearchData } from '@react-v1/mock-data'

const businesses = transformBusinesses(yelpBusinessSearchData.businesses)

describe('filterBusinesses', () => {
  it('should return all businesses when there is no filter applied', () => {
    const filteredBusinesses = filterBusinesses(businesses, null)
    expect(filteredBusinesses).toHaveLength(20)
  });

  it('should filter businesses based on category words', () => {
    const filteredBusinesses = filterBusinesses(businesses, 'Tea')
    expect(filteredBusinesses).toHaveLength(1)
  });

  it('should filter businesses based on category', () => {
    const filteredBusinesses = filterBusinesses(businesses, 'Coffee & Tea')
    expect(filteredBusinesses).toHaveLength(1)
  });

  it('should filter businesses based on words in the business name', () => {
    const filteredBusinesses = filterBusinesses(businesses, 'Boeuf')
    expect(filteredBusinesses).toHaveLength(1)
  });

  it('should not be case sensitive', () => {
    const filteredBusinesses = filterBusinesses(businesses, 'barbeque')
    expect(filteredBusinesses).toHaveLength(1)
  })
});
