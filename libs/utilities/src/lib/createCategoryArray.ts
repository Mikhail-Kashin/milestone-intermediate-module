import { Business } from '@react-v1/types';

export const createCategoryArray = (businesses: Business[]): string[] | [] => {
  const categoryArray: string[] = [];

  businesses.forEach((business) => {
    business.categories.map((category) => {
      if (categoryArray.includes(category)) return;
      else {
        categoryArray.push(category);
      }
    });
  });
  return categoryArray as string[];
};
